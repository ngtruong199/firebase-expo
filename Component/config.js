// import * as firebase from "firebase";

// var firebaseConfig = {
//   apiKey: "AIzaSyCofp5xQJlxga3rVDZlEBIn-x7Q7UdmXfg",
//   authDomain: "benhan-e5b9f.firebaseapp.com",
//   projectId: "benhan-e5b9f",
//   storageBucket: "benhan-e5b9f.appspot.com",
//   messagingSenderId: "532408129931",
//   appId: "1:532408129931:web:18e2a5f48797c6d86f778e",
//   measurementId: "G-JW6D45BG9E",
// };
// // Initialize Firebase
// export const firebase = firebase.initializeApp(firebaseConfig);
// // //  firebase.analytics();

import firebase from "firebase";
import "firebase/firestore";
import "firebase/storage";

var firebaseConfig = {
  apiKey: "AIzaSyCofp5xQJlxga3rVDZlEBIn-x7Q7UdmXfg",
  authDomain: "benhan-e5b9f.firebaseapp.com",
  projectId: "benhan-e5b9f",
  storageBucket: "benhan-e5b9f.appspot.com",
  messagingSenderId: "532408129931",
  appId: "1:532408129931:web:18e2a5f48797c6d86f778e",
  measurementId: "G-JW6D45BG9E",
};

// Initialize Firebase

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
firebase.firestore().enablePersistence();
const storage = firebase.storage();
export default {
  firebase,
  db,
  storage,
};
