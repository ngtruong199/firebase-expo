import React, { useEffect, useState } from "react";
import { View, Text, ScrollView, Button } from "react-native";

import firebase from "../Component/config.js";
import { ListItem } from "react-native-elements";
const userList = (props) => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    firebase.db.collection("users").onSnapshot((querySnapshot) => {
      const users = [];

      querySnapshot.docs.forEach((doc) => {
        const { name, email, phone } = doc.data();
        users.push({
          id: doc.id,
          name,
          email,
          phone,
        });
      });
      setUsers(users);
    });
  }, []);

  return (
    <View>
      <ScrollView>
        <Button
          title="Create User"
          onPress={() => props.navigation.navigate("CreateUser")}
        />
        {users.map((user) => {
          return (
            <ListItem
              key={user.id}
              bottomDivider
              onPress={() => {
                props.navigation.navigate("UserDetail", {
                  userId: user.id,
                });
              }}
            >
              <ListItem.Chevron />
              <ListItem.Content>
                <ListItem.Title>{user.name}</ListItem.Title>
                <ListItem.Subtitle>{user.email}</ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default userList;
