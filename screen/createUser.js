import React, { useState } from "react";
import {
  View,
  Text,
  Button,
  TextInput,
  ScrollView,
  StyleSheet,
} from "react-native";
import firebase from "../Component/config.js";
const createUser = (props) => {
  const [state, setstate] = useState({
    name: "",
    email: "",
    phone: "",
  });
  const handleChangeText = (name, value) => {
    setstate({ ...state, [name]: value });
  };

  const saveNewUser = () => {
    // console.log(state);
    if (state.name === "") {
      alert("khong rong");
    } else {
      try {
        firebase.db.collection("users").add({
          name: state.name,
          email: state.email,
          phone: state.phone,
        });
        props.navigation.navigate("UserList");
      } catch (error) {
        console.log(error);
      }
    }
  };
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.inputG}>
          <TextInput
            placeholder="Name User"
            onChangeText={(value) => handleChangeText("name", value)}
          />
        </View>
        <View style={styles.inputG}>
          <TextInput
            placeholder="Email User"
            onChangeText={(value) => handleChangeText("email", value)}
          />
        </View>
        <View style={styles.inputG}>
          <TextInput
            placeholder="Phone User"
            onChangeText={(value) => handleChangeText("phone", value)}
          />
        </View>
        <View style={styles.inputG}>
          <Button title="Save" onPress={() => saveNewUser()} />
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputG: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
  },
});
export default createUser;
