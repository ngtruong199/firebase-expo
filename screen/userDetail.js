import React, { useEffect, useState } from "react";

import {
  View,
  Text,
  StyleSheet,
  Button,
  TextInput,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import firebase from "../Component/config.js";

const userDetail = (props) => {
  const initialState = {
    id: "",
    name: "",
    email: "",
    phone: "",
  };
  //lấy lại ds cũ
  const [user, setuser] = useState(initialState);
  //load lại date từ bên userlist qua
  const [loading, setloading] = useState(true);

  //tìm theo id của ds
  const getUserById = async (id) => {
    const dbRef = firebase.db.collection("users").doc(id);
    const doc = await dbRef.get();
    const user = doc.data();
    setuser({
      ...user,
      id: doc.id,
    });
    setloading(false);
  };

  useEffect(() => {
    getUserById(props.route.params.userId);
  }, []);
  const handleChangeText = (name, value) => {
    setuser({ ...user, [name]: value });
  };

  if (loading) {
    return (
      <View>
        <ActivityIndicator size="large" color="#9e9e9e" />
      </View>
    );
  }
  const uploadUser = async () => {
    const dbRef = firebase.db
      .collection("users")
      .doc(props.route.params.userId);
    await dbRef.set({
      name: user.name,
      email: user.email,
      phone: user.phone,
    });
    setuser(initialState);
    props.navigation.navigate("UserList");
  };
  const deleteUser = async () => {
    const dbRef = firebase.db
      .collection("users")
      .doc(props.route.params.userId);
    await dbRef.delete();
    props.navigation.navigate("UserList");
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.inputG}>
          <TextInput
            placeholder="Name User"
            value={user.name}
            onChangeText={(value) => handleChangeText("name", value)}
          />
        </View>
        <View style={styles.inputG}>
          <TextInput
            placeholder="Email User"
            value={user.email}
            onChangeText={(value) => handleChangeText("email", value)}
          />
        </View>
        <View style={styles.inputG}>
          <TextInput
            placeholder="Phone User"
            value={user.phone}
            onChangeText={(value) => handleChangeText("phone", value)}
          />
        </View>
        <View style={styles.inputG}>
          <Button title="Update" onPress={() => uploadUser()} />
        </View>
        <View>
          <Button color="#9e9e" title="Delete" onPress={() => deleteUser()} />
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputG: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
  },
});
export default userDetail;
