import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();
import userList from "./screen/userList.js";
import userDetail from "./screen/userDetail.js";
import createUser from "./screen/createUser.js";

function MyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="UserList"
        component={userList}
        options={{ title: "users List" }}
      />
      <Stack.Screen
        name="CreateUser"
        component={createUser}
        options={{ title: "create User" }}
      />
      <Stack.Screen
        name="UserDetail"
        component={userDetail}
        options={{ title: "user Detail" }}
      />
    </Stack.Navigator>
  );
}
export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}
const styles = StyleSheet.create({});
