import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import * as ImagePicker from "expo-image-picker";
import firebase from "./Component/config";
import { TextInput } from "react-native";
// const Stack = createStackNavigator();
// import userList from "./screen/userList.js";
// import userDetail from "./screen/userDetail.js";
// import createUser from "./screen/createUser.js";
// import { Button } from "react-native";

// function MyStack() {
//   return (
//     <Stack.Navigator>
//       <Stack.Screen name="UserList" component={userList} options={{title:'users List'}} />
//       <Stack.Screen name="CreateUser" component={createUser} options={{title:'create User'}}/>
//       <Stack.Screen name="UserDetail" component={userDetail} options={{title:'user Detail'}}/>
//     </Stack.Navigator>
//   );
// }
export default function App() {
  const [image, setImage] = useState();
  const [uploading, setUploading] = useState(false);
  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const {
          status,
        } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  const uploadImage = async () => {
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function () {};
      xhr.responseType = "blob";
      xhr.open("GET", image, true);
      xhr.send(null);
    });
    const ref = firebase.storage.ref().child(new Date().toISOString());
    const snapshot = ref.put(blob);
    snapshot.on(
      "state_changed",
      (snapshot) => {},
      (error) => {
        setUploading(false);
        console.log(error);
        blob.close();
        return;
      },
      () => {
        snapshot.snapshot.ref.getDownloadURL().then((url) => {
          setUploading(false);
          console.log("Download URL: ", url);
          blob.close();
          return url;
        });
      }
    );
  };

  return (
    <View>
      <Image source={{ uri: image }} style={{ width: 300, height: 300 }} />

      <Button title="Chon hình" onPress={pickImage} />
      <Button title="upload" onPress={uploadImage} />
    </View>
  );
}
const styles = StyleSheet.create({});
